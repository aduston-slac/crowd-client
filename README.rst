=====================================================
Python REST Client for Atlassian Crowd Administration
=====================================================

This module utilizes the Crowd REST API to allow automation of a variety of account administration functions. 
This is by no means a comprehensive implementation of the Crowd API features. I will continue to to add more 
functionality as time permits and circumstances demand. 

In order to authenticate with Crowd using this module you will need to create an application within crowd that
has access to the user database you wish to manage. 

This client is built using the fantastic Requests_ library. 

.. _Requests: http://docs.python-requests.org/en/latest/

Installing
==========

.. This package is currently hosted in a private Bitbucket repository owned by Adam Duston until I decide on a better solution. To obtain access please e-mail aduston@slac.stanford.edu

To install the package from git using pip use the following command::

    pip install git+ssh://git@bitbucket.org/aduston-slac/crowd-client.git

.. Note that at SLAC pip install requires the --user flag to install in user space. See
   https://confluence.slac.stanford.edu/display/~aduston/Installing+Custom+Python+Packages+on+SLAC+machines


Usage
===== 

A quick start example for the impatient::


        from crowd.client import CrowdClient

        server = "https://crowd.example.com/crowd"
        
        auth = ('username', 'password')

        crowd = CrowdClient(server, auth)

        crowd.user_exists("hsimpson") # True
        
        crowd.get_user("hsimpson") # Dictionary of user details (see Crowd API docs for details)
        
        crowd.user_groups("hsimpson") # ['springfield', 'moes-regulars', 'simpsons-family']
        
        crowd.deactivate_user("hsimpson") # True (account succesfully disabled)

        crowd.activate_user("hsimpson") # True (account succesfully enabled)

        crowd.add_group_members("simpsons-family", ['mbouvier'])

The docstrings in the code contain more detail about the available functions and their usage, but this should 
at least get you started. 


About
=====

This Crowd API client module is maintained by Adam Duston for use by the Enterprise Applications Administration team 
at SLAC National Accelerator Laboratory.

For more details on the Crowd rest API see Atlassian's documentation_.

.. _documentation: https://developer.atlassian.com/display/CROWDDEV/Overview+of+the+Crowd+REST+APIs