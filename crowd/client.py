#!/usr/bin/env python
""" 
.. module:: crowd.client
    :platform: Unix
    :synopsyis: Python REST Client for Atlassian Crowd account administration.

.. moduleauthor:: Adam Duston <aduston@slac.stanford.edu>

"""

import logging
import requests 
import json 

class CrowdClient(object):
    """ Python client for Crowd REST API 
    
    This client contains useful tools for account administration in Crowd,
    but by no means is a comprehensive implementation of the Crowd API.
    I've added what I think will be useful in the short term, and intend
    to add more functions and features as time allows and circumstances demand.
    """ 

    def __init__(self, server=None, basic_auth=None, logger=None):
        """ 
        Initialize the client instance.  
        
        :param server: The url of the Crowd application you wish to interact with.
        :param basic_auth: A tuple containing the username and password to use for
                          basic authentication. 
        """
        self._headers = {'Content-Type' : 'application/json',
                         'Accept' : 'application/json' }
        # Ultimately the api part of the url shouldn't be hard coded, but
        # for the sake of expediency I'll do it until it becomes a problem.
        self._baseurl = "%s/%s" % (server, 'rest/usermanagement/1')
        self._auth = basic_auth
        self.logger = logger or logging.getLogger(__name__)

    def _rest_get(self, url):
        """ 
        Perform GET from given API resource. 
        
        :param url: The api resource url to post the payload to. 
        """
        full_url = "%s/%s" % (self._baseurl, url) 
        self.logger.debug('rest_get %s', full_url)
        response = requests.get(full_url, auth=self._auth, headers=self._headers)
        self.logger.debug('response: %s', response)
        
        return response

    def _rest_post(self, url, payload):
        """ 
        Perform HTTP post with given ``payload``.
        
        :param url: The api resource url to post the payload to. 
        :param payload: The json-formatted payload to post.
        """
        full_url = "%s/%s" % (self._baseurl, url) 
        self. logger.debug('rest_post %s : %s', full_url, payload)
        response = requests.post(full_url, auth=self._auth, headers=self._headers,
                                 data=json.dumps(payload))
        self.logger.debug('response: %s', response)
        
        return response
    
    def _rest_put(self, url, payload):
        """ 
        Perform HTTP put with given ``payload``.
        
        :param url: The api resource url to post the payload to. 
        :param payload: The json-formatted payload to post.
        """
        full_url = "%s/%s" % (self._baseurl, url) 
        self.logger.debug('rest_put %s : %s', full_url, payload)
        response = requests.put(full_url, auth=self._auth, headers=self._headers,
                                 data=json.dumps(payload))
        self.logger.debug('response: %s', response)
        
        return response
    
    def _rest_delete(self, url, payload):
        """ 
        Perform HTTP DELETE with the given ``payload``. 
        
        
        :param url: The api resource url to post the payload to. 
        :param payload: The json-formatted payload to post.
        """
        full_url = "%s/%s" % (self._baseurl, url)
        self.logger.debug('rest_delete %s : %s', full_url, payload)
        response = requests.delete(full_url, auth=self._auth, headers=self._headers,
                                   data=json.dumps(payload))
        self.logger.debug('response: %s', response)
        
        return response

    def user_exists(self, uid):
        """ 
        Search for the given ``uid``. 
        If the user exists returns true, if not false.
        
        :param uid: Username for the crowd account.
        :type uid: str.
        :returns: bool - True if user exists, False if not. 
        """ 
        return_value = False
        api_url = "user?username=%s" % uid
        response = self._rest_get(api_url)
        if response.status_code == requests.codes.ok:
            return_value = True
        else: 
            self.logger.debug("User account %s not found.", uid)

        return return_value 

    def get_user(self, uid):
        """ 
        Retrieve account details for given ``username``. 

        :param uid: Username for the crowd account.
        :type uid: str.
        :returns: dict -- Account details for user. Empty if no account exists. 
        """ 
        details = {}
        api_url = "user?username=%s" % uid 
        response = self._rest_get(api_url)
        if response.status_code == requests.codes.ok:
            details = response.json()
        else:
            self.logger.error("User account %s not found", uid) 
        
        return details

    def get_group(self, group):
        """
        Retrieve ``group`` details. 

        :param group: Name of the group to get. 
        :type group: str
        :returns: dict - Group details as dictionary. 
        """
        details = {}
        api_url = "group?groupname=%s" % group
        response = self._rest_get(api_url) 
        if response.status_code == requests.codes.ok:
            details = response.json()
        else:
            r = response.json()
            self.logger.error("Could not retrieve group: %s, %s", 
                              r['reason'], r['message'])

        return details


    def get_group_members(self, group): 
        """ 
        For given ``group`` return a list of direct members. 
        
        :param group: Name of the Crowd group to search for. 
        :type group: str.
        :returns: list -- Usernames of group members. 
        """
        members = []
        self.logger.debug("Getting users in group %s", group) 
        ## Sometimes groups have lots of members, but the API by default
        ## Will only return the first 1000. Iterate until the whole group
        ## is fetched, 1000 members at a time. 
        index = 0
        more_members = True
        while more_members:
            if index == 0:
                api_url = "group/user/direct?groupname=%s" % group
            else:
                api_url = "group/user/direct?groupname=%s&start-index=%s" % (group,index)
            response = self._rest_get(api_url)
            if response.status_code == requests.codes.ok:
                data = response.json()
                new_members = [i['name'] for i in data['users']]
                if len(new_members) == 0:
                    # No more results. We're done here. 
                    return members
                else: 
                    members = members + new_members
                    index += 1000
            elif response.status_code == requests.codes.not_found:
                self.logger.info("Requested group %s does not exist", group)
        
        return members

    def get_user_groups(self, uid):
        """ 
        Return a list of groups in which ``uid`` is a direct member. 
        
        :param uid: Username for the crowd account.
        :type uid: str.
        :returns: list -- Groups of which given Username is a direct member. 
        """
        groups = None
        api_url = "user/group/direct?username=%s" % uid
        self.logger.debug("Getting groups for %s", uid)
        response = self._rest_get(api_url)
        if response.status_code == requests.codes.ok:
            data = response.json()
            groups = [i['name'] for i in data['groups']]
        
        return groups

    def update_user(self, details):
        """ 
        Update the user account with the given details. 

        :param details: Account details for a Crowd user. Same as output of user_detail()
        :type details: dict.
        :returns: bool - True for succesful update, False for an error or failure. 
        """
        uid = details['name']
        api_url = "user?username=%s" % uid
        response = self._rest_put(api_url, details)
        if response.status_code == requests.codes.no_content:
            return True
        elif response.status_code == requests.codes.not_found:
            self.logger.error("Could not update user %s - Account does not exist.", uid)
        else:
            r = response.json()
            self.logger.error("Update account failed: %s, %s", r['reason'], r['message'])

        return False
    
    def deactivate_user(self, uid):
        """ 
        Update user account with username ``uid`` and set the active flag to False. 
            
        
        :param uid: Username for the crowd account.
        :type uid: str.
        :returns: bool - True for success, False if something bad happens.
        """
        details = self.get_user(uid)
        if details:
            api_url = "user?username=%s" % uid
            details['active'] = False
            return self.update_user(details) 

        return False 
    
    def activate_user(self, uid):
        """
        Update user account with username ``uid`` and set the active flag to True.

        :param uid: Username for the crowd account. 
        :type uid: str.
        :returns: bool - True for success, False if not. 
        """
        details = self.get_user(uid)
        if details:
            api_url = "user?username=%s" % uid 
            details['active'] = True
            return self.update_user(details)

        return False 

    def add_group_members(self, group, add_users):
        """ 
        Update ``group`` with the given users. 

        :param group: Name of group to update
        :type group: str.
        :param add_users: List of users to add to group.
        :type add_users: list.
        """
        api_url = "group/user/direct?groupname=%s" % group
        for user in add_users:
            payload = {'name':user}
            response = self._rest_post(api_url,payload) 
            if response.status_code == requests.codes.created:
                self.logger.info("Succesfully added %s to %s", user, group)
            elif response.status_code == requests.codes.bad_request:
                self.logger.error("Could not add %s to %s: user does not exist.",user, group)
            elif response.status_code == requests.codes.not_found:
                # If 404 is returned the group doesn't exist, so bail out of the 
                # function to avoid trying a bunch more updates that will surely fail.
                self.logger.error("Group %s does not exist. Abandoning group updates.", group)
                return
            elif response.status_code == requests.codes.conflict:
                self.logger.error("User %s already in %s", user, group)
            else:
                r = response.json()
                self.logger.error("Something unexpected happend: %s, %s",
                                  r['reason'], r['message'])

    def remove_group_members(self, group, remove_users):
        """ 
        Remove given users from ``group``. 

        :param group: Name of group to update
        :type group: str.
        :param remove_users: List of users to remove from group
        :type remove_users: list.
        """
        api_url = "group/user/direct?groupname=%s" % group
        for user in remove_users:
            delete_url = "%s&username=%s" % (api_url, user) 
            response = self._rest_delete(delete_url,{}) 
            if response.status_code == requests.codes.no_content:
                self.logger.info("Succesfully removed %s from %s", user, group)
            elif response.status_code == requests.codes.not_found:
                r = response.json()
                if r['reason'] == 'GROUP_NOT_FOUND':
                    self.logger.error("Group %s does not exist. Abandoning group updates.", 
                                      group)
                    return
                elif r['reason'] == 'MEMBERSHIP_NOT_FOUND':
                    self.logger.error("Cannot remove %s from %s - user not in group",
                                       user, group)
            else:
                r = response.json()
                self.logger.error("Something unexpected happend: %s", r)
