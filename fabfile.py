from fabric.api import local
import os
import os.path

def virtualenv():
    """ If no virtualenv exists, create a new one. """
    if not os.path.exists('lib/python2.7/site.py'):
        local('virtualenv .')

def clean_virtualenv():
    """ Clean up the virtualenv """
    local('rm -rf bin include lib lib43 share src build dist .pipped')
    local('rm -rf *.egg-info')

def clean():
    """ Tell git to clean untracked files. """
    local('git clean -df')

def clean_all():
    """ Clean everything that can be cleaned. """
    clean_virtualenv()
    clean()

def freeze():
    """ Freeze libraries into requirements.txt """ 
    local('. bin/activate && pip freeze > requirements.txt')

def setup():
    """ Create a new virtulenv and install the package and dependencies """ 
    if not os.path.exists('.pipped'):
        virtualenv()
        local('. bin/activate && pip install -r requirements.txt')
        local('touch .pipped')
    if (os.path.getmtime('.pipped') > os.path.getmtime('requirements.txt')):
        local('. bin/activate && pip install -r requirements.txt')

