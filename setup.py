#!/usr/bin/env python
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

packages = ['crowd']
requires = ['requests']

with open('README.rst', 'r') as f:
    readme = f.read()

data = dict(
    name='crowd',
    author='Adam Duston',
    author_email='aduston@slac.stanford.edu',
    description= ('A client for account administartion via the Atlassian Crowd REST Api'),
    long_description=readme,
    url='https://bitbucket.org/aduston-slac/crowd-client',
    version=0.1,
    packages=packages,
    package_dir={'crowd': 'crowd'},
    include_package_data=True,
    install_requires=requires,
    license='Apache 2.0',
    classifiers=(
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Systems Administrators',
        'Natural Language :: English',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python 2.7', )
       
)

setup(**data)
